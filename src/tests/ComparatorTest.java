package tests;

import graham.AngleComparator;
import graham.Point;

import java.util.Comparator;

public class ComparatorTest {
    public static void main(String[] args) {
        Point a = new Point(1, 4);
        Comparator<Point> comparator = new AngleComparator(a);
        System.out.println(comparator.compare(new Point(1,6),new Point(2,2)));
        System.out.println(comparator.compare(new Point(6,6),new Point(9,7)));
        System.out.println(comparator.compare(new Point(11,8),new Point(7,8)));
        System.out.println(comparator.compare(new Point(3,4),new Point(11,4)));

    }
}

