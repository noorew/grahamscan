package tests;

import java.util.Arrays;
import java.util.Stack;

public class Test {
    public static void main(String[] args) {
        Stack<String> ss = new Stack<>();
        ss.push("one");
        ss.push("two");
        ss.push("three");
        ss.push("four");
        ss.push("five");
        System.out.println(ss.get(ss.size() - 1));
        System.out.println(Arrays.toString(ss.toArray()));
    }
}
