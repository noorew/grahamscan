package graham;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;

public class InputHandler {

    public static final int INCREMENT = 200;

    private static void generateFile() {
        try {
            File file = new File("src/files/input2.txt");
            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            Random random = new Random();
            for (int i = 1; i < 90; i++) {
                for (int j = 0; j < i * INCREMENT; j++) {
                    String str = Integer.toString(random.nextInt(10000));
                    fw.write(str + " ");
                }
                fw.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException();
        }
    }

    public static LinkedList<Point> createListOfPoint(String str) {
        LinkedList<Point> a = new LinkedList<>();
        int j = 0;
        for (int i = 0; i < str.split(" ").length / 2; i++) {
            a.push(new Point(Integer.parseInt(str.split(" ")[j]), Integer.parseInt(str.split(" ")[j + 1])));
            j += 2;
        }
        return a;
    }

    public static Point[] createArrayOfPoint(String str) {
        Point[] a = new Point[str.split(" ").length / 2];
        int j = 0;
        for (int i = 0; i < a.length; i++) {
            a[i] = new Point(Integer.parseInt(str.split(" ")[j]),
                    Integer.parseInt(str.split(" ")[j + 1]));
            j += 2;
        }
        return a;
    }

    public static Point[] transferLinkedListToArray(LinkedList<Point> linkedList) {
        Point[] arr = new Point[linkedList.size()];
        Object[] a = linkedList.toArray();
        for (int i = 0; i < a.length; i++) {
            arr[i] = (Point) a[i];
        }
        return arr;
    }
}
