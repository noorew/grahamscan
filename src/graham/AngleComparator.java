package graham;

import java.util.Comparator;

public class AngleComparator implements Comparator<Point> {
    Point point;

    public AngleComparator(Point point) {
        this.point = point;
    }

    @Override
    public int compare(Point o1, Point o2) {
        if (o1.equals(o2)) {
            return 0;
        }
        if (o1.getX() == point.getX() || o2.getX() == point.getX()) {
            if (o1.getX() == point.getX() && o2.getX() == point.getX()) {
                if (o1.getY() == o2.getY()) {
                    return 0;
                } else {
                    return (o1.getY() - point.getY() > o2.getY() - point.getY()) ? -1 : 1;
                }
            } else {
                if (o1.getX() == point.getX()) {
                    return (o1.getY() > point.getY()) ? 1 : -1;
                } else {
                    return (o2.getY() > point.getY()) ? -1 : 1;
                }
            }
        }

        double tgA = (double) (o1.getY() - point.getY()) / (double) (o1.getX() - point.getX());
        double tgB = (double) (o2.getY() - point.getY()) / (double) (o2.getX() - point.getX());

        if (tgA == tgB) {
            return Math.sqrt(((o1.getY() - point.getY()) * (o1.getY() - point.getY())) +
                    ((o1.getX() - point.getX()) * (o1.getX() - point.getX()))) >
                    Math.sqrt(((o2.getY() - point.getY()) * (o2.getY() - point.getY())) +
                            ((o2.getX() - point.getX()) * (o2.getX() - point.getX()))) ? 1 : -1;
        }
        return (tgA > tgB) ? 1 : -1;
    }
}
