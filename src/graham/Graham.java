package graham;

import graham.AngleComparator;

import java.util.Arrays;
import java.util.Stack;


public class Graham {
    public static long iterAmount = 0;

    /**
     * Возвращает значение < 0, если точка c левее относительно прямой ab,
     * > 0, если точка c правее относительно прямой ab　и 0, если находится на прямой.
     */
    private static double rotate(Point a, Point b, Point c) {
        iterAmount++;
        return (b.getX() - a.getX()) * (c.getY() - b.getY()) - (b.getY() - a.getY()) * (c.getX() - b.getX());
    }

    private static Point[] sort(Point[] arr) {
        int l = 0;
        Point min = arr[l];
        for (int i = 0; i < arr.length; i++) {
            iterAmount++;
            if (min.getX() >= arr[i].getX()) {
                if (min.getX() == arr[i].getX()) {
                    if (min.getY() > arr[i].getY()) {
                        min = arr[i];
                        l = i;
                    }
                } else {
                    min = arr[i];
                    l = i;
                }
            }
        }
        Point tmp = arr[0];
        arr[0] = min;
        arr[l] = tmp;

        AngleComparator angleComparator = new AngleComparator(min);
        Arrays.sort(arr, 1, arr.length, angleComparator);
        iterAmount *= (arr.length * Math.log(arr.length));

        return arr;
    }

    public static Stack<Point> grahamScan(Point[] arr) {
        arr = sort(arr);
        Stack<Point> stack = new Stack<>();
        stack.push(arr[0]);
        stack.push(arr[1]);
        for (int i = 2; i < arr.length; i++) {
            while (stack.size() > 1 && rotate(stack.get(stack.size() - 2), stack.get(stack.size() - 1), arr[i]) <= 0) {
                iterAmount++;
                stack.pop();
            }
            iterAmount++;
            stack.push(arr[i]);
        }
        return stack;
    }
}
